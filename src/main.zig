const std = @import("std");
const rl = @import("raylib");
const game = @import("game.zig");

pub fn main() !void {
    // Setup.
    try game.init();
    defer game.cleanup() catch 1;
    defer rl.closeWindow();

    // Main game loop
    var run = true;
    while (!rl.windowShouldClose() and run) { // Detect window close button or ESC key
        // Update
        run = game.logic();

        // Draw
        //----------------------------------------------------------------------------------
        rl.beginDrawing();
        defer rl.endDrawing();

        rl.clearBackground(rl.Color.white);
        try game.draw();
        //----------------------------------------------------------------------------------
    }
}
