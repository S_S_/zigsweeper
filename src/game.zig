const rl = @import("raylib");
const std = @import("std");

var gpa = std.heap.GeneralPurposeAllocator(.{}){};
const allocator = gpa.allocator();

// Some structs.
pub const game_data = struct {
    started: bool,
    board: []tile,
    dim_x: u8,
    dim_y: u8,
};

const tile = struct {
    state: tile_state,
    x: u8,
    y: u8,
    mine: bool,
    adj: u8,
};

const tile_state = enum {
    hidden,
    revealed,
    flagged,
    sus,
};

const point = struct {
    x: u8,
    y: u8,
};

const point_node = struct {
    val: point,
    next: ?*point_node,
};

// Zero intiialise the slow way since `zeroes` is apparently bad.
pub var data: game_data = .{
    .started = false,
    .board = undefined,
    .dim_x = 0,
    .dim_y = 0,
};

// Set everything up for the game.
pub fn init() !void {
    // Start with some Raylib initialisation.
    const screenWidth = 800;
    const screenHeight = 450;

    rl.initWindow(screenWidth, screenHeight, "Minesweeper");

    rl.setTargetFPS(60); // Set our game to run at 60 frames-per-second

    // For now, start a game with forced mine counts.
    try init_game(10, 10, 10);
}

pub fn handle_input() bool {
    var out = true;
    // Simple and mouse-driven.
    const mouse_x = @divTrunc(rl.getMouseX(), 25);
    const mouse_y = @divTrunc(rl.getMouseY(), 25);
    const pos: u16 = @intCast(mouse_y * data.dim_x + mouse_x);
    if (rl.isMouseButtonReleased(rl.MouseButton.mouse_button_left) and !(data.board[pos].state == tile_state.flagged)) {
        std.debug.print("{} {}: {}\n", .{ mouse_x, mouse_y, pos });
        // Get revealing:
        tile_reveal(@intCast(mouse_x), @intCast(mouse_y));
        if (data.board[pos].mine) {
            out = false;
        }
    }
    if (rl.isMouseButtonReleased(rl.MouseButton.mouse_button_right)) {
        if (data.board[pos].state == tile_state.hidden) {
            data.board[pos].state = tile_state.flagged;
        } else if (data.board[pos].state == tile_state.flagged) {
            data.board[pos].state = tile_state.sus;
        } else if (data.board[pos].state == tile_state.sus) {
            data.board[pos].state = tile_state.hidden;
        }
    }
    return out;
}

pub fn logic() bool {
    return handle_input();
}

pub fn draw() !void {
    // Drawn known info.
    for (0..data.dim_x) |i| {
        for (0..data.dim_y) |j| {
            const pos = j * data.dim_x + i;
            switch (data.board[pos].state) {
                tile_state.hidden => {
                    continue;
                },
                tile_state.flagged => {
                    rl.drawRectangle(@intCast(25 * i), @intCast(25 * j), @intCast(25), @intCast(25), rl.Color.red);
                },
                tile_state.sus => {
                    rl.drawRectangle(@intCast(25 * i), @intCast(25 * j), @intCast(25), @intCast(25), rl.Color.yellow);
                },
                else => {
                    rl.drawRectangle(@intCast(25 * i), @intCast(25 * j), @intCast(25), @intCast(25), rl.Color.sky_blue);
                    if (data.board[pos].adj > 0) {
                        rl.drawText(&[2:0]u8{ data.board[pos].adj + '0', 0 }, @intCast(25 * i + 10), @intCast(25 * j + 6), 20, rl.Color.black);
                    }
                },
            }
        }
    }
    // Draw a grid.
    for (0..data.dim_x - 1) |i| {
        rl.drawLine(@intCast((i + 1) * 25), @intCast(0), @intCast((i + 1) * 25), @intCast(data.dim_y * 25), rl.Color.black);
    }
    for (0..data.dim_y - 1) |i| {
        rl.drawLine(@intCast(0), @intCast((i + 1) * 25), @intCast(data.dim_x * 25), @intCast((i + 1) * 25), rl.Color.black);
    }
}

pub fn cleanup() !void {
    allocator.free(data.board);
}

fn init_game(x: u8, y: u8, mines: u16) !void {
    var prng = std.rand.DefaultPrng.init(blk: {
        var seed: u64 = undefined;
        try std.posix.getrandom(std.mem.asBytes(&seed));
        break :blk seed;
    });
    // Create the board.
    data.dim_x = x;
    data.dim_y = y;
    data.board = try allocator.alloc(tile, x * y);
    // Place mines.
    const rand = prng.random();
    var n_free: u16 = x * y;
    var n = mines;
    var head: point_node = .{
        .val = undefined,
        .next = null,
    };
    defer while (head.next != null) {
        const e: *point_node = head.next orelse break;
        //std.debug.print("{d} {d}\n", .{ e.val.x, e.val.y });
        head.next = e.next;
        allocator.destroy(e);
    };
    for (0..x) |i| {
        for (0..y) |j| {
            // Init the board tiles and create the list in the same loop.
            // Board - using offsets.
            data.board[(i * x) + j].x = @intCast(j);
            data.board[(i * x) + j].y = @intCast(i);
            data.board[(i * x) + j].mine = false;
            data.board[(i * x) + j].state = tile_state.hidden;
            data.board[(i * x) + j].adj = 0;
            // List
            const p: *point_node = try allocator.create(point_node);
            p.val.x = @intCast(j);
            p.val.y = @intCast(i);
            p.next = head.next;
            head.next = p;
        }
    }
    while (n > 0) : (n -= 1) {
        var idx = rand.intRangeAtMost(u16, 0, n_free - 1);
        //std.debug.print("{}\n", .{idx});
        // Iterate to the object before the one we want, so that we can pop it.
        var p: *point_node = &head;
        while (idx > 0) : (idx -= 1) {
            p = p.next.?;
        }
        const tmp: *point_node = p.next.?;
        //std.debug.print("Mine at: {d} {d}\n", .{ p.next.?.val.x, p.next.?.val.y });
        data.board[(x * tmp.val.y) + tmp.val.x].mine = true;
        p.next = tmp.next;
        allocator.destroy(tmp);
        n_free -= 1;
    }
    // Print out the board as a test.
    for (0..y) |i| {
        for (0..x) |j| {
            const out: u8 = switch (data.board[x * i + j].mine) {
                true => 'M',
                false => ' ',
            };
            std.debug.print("{c}", .{out});
        }
        std.debug.print("\n", .{});
    }
    data.started = true;

    // Resize the window to fit the game.
    rl.setWindowSize(x * 25, y * 25);
}

// Reveal tiles on click. Calls itself recursively for chained reveals.
fn tile_reveal(x: u8, y: u8) void {
    const pos = data.dim_x * y + x;
    const xmin = if (x > 0) x - 1 else 0;
    const xmax = if (x < data.dim_x - 1) x + 2 else data.dim_x;
    const ymin = if (y > 0) y - 1 else 0;
    const ymax = if (y < data.dim_y - 1) y + 2 else data.dim_y;
    if (data.board[pos].mine or data.board[pos].state == tile_state.revealed) {
        // Tile contains a mine or is already revealed.
        return;
    }
    data.board[pos].state = tile_state.revealed;
    for (xmin..xmax) |p| {
        for (ymin..ymax) |q| {
            if (data.board[q * data.dim_x + p].mine) {
                data.board[pos].adj += 1;
            }
        }
    }
    if (data.board[pos].adj == 0) {
        for (xmin..xmax) |p| {
            for (ymin..ymax) |q| {
                if (p == x and q == y) {
                    continue;
                }
                if (data.board[q * data.dim_x + p].state == tile_state.hidden) {
                    tile_reveal(@intCast(p), @intCast(q));
                }
            }
        }
    }
    return;
}
